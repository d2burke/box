//
//  ViewController.h
//  Box
//
//  Created by Daniel.Burke on 1/27/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) NSString *userType;

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *enginnerButton;
@property (weak, nonatomic) IBOutlet UIButton *designerButton;
@property (weak, nonatomic) IBOutlet UIButton *marketerButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

- (IBAction)chooseType:(id)sender;
- (IBAction)registerNew:(id)sender;
@end

