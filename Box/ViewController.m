//
//  ViewController.m
//  Box
//
//  Created by Daniel.Burke on 1/27/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _enginnerButton.layer.cornerRadius = 50.f;
    _designerButton.layer.cornerRadius = 50.f;
    _marketerButton.layer.cornerRadius = 50.f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)chooseType:(id)sender {
    UIButton *button = (UIButton*)sender;
    if([button.titleLabel.text isEqualToString:@"E"]){
        _userType = @"Engineer";
        NSLog(@"Chose: %@", _userType);
    }
    else if([button.titleLabel.text isEqualToString:@"D"]){
        _userType = @"Designer";
    }
    else if([button.titleLabel.text isEqualToString:@"M"]){
        _userType = @"Marketer";
    }
}

- (IBAction)registerNew:(id)sender {
    NSLog(@"%@", _userType);
    NSLog(@"%@", _nameField.text);
    NSLog(@"%@", _emailField.text);
    NSLog(@"%@", _passwordField.text);
    
    [self performSegueWithIdentifier:@"UserDidRegister" sender:self];
}
@end
